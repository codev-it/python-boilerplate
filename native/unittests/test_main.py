# -*- coding: utf-8 -*-

import unittest2

from BOILERPLATE import __main__


class MainTestCase(unittest2.TestCase):
    """UnitTestCase
    BOILERPLATE.__main__
    """

    def test_main(self):
        """UnitTest
        BOILERPLATE.__main__.main()
        """
        with self.assertRaises(SystemExit) as cm:
            __main__.main()

        self.assertEqual(cm.exception.code, 0)


if __name__ == '__main__':
    unittest2.main()
