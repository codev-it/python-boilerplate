.. image:: https://www.codev-it.at/logo.png
   :target: https://www.codev-it.at

Codev-IT Python Boilerplate
###########################

.. image:: https://img.shields.io/badge/License-MIT-green.svg
   :target: https://bitbucket.org/codev-it/python-boilerplate/src/master/native/LICENSE

Description
===========

This Python boilerplate is a basic template for setting up a Python project. It includes a requirements file that specifies the necessary packages for different stages of development, such as setup, installation, development, documentation, and Debian packaging.

Requirements
------------

The **requirements.txt** file contains different sections for various dependencies:

- **setup_requires**: Packages needed for the initial setup of the project.
- **install_requires**: Essential packages required to run the project.
- **extras_require**: Additional dependencies for development (dev), documentation (doc), and Debian packaging (deb).

Installation
------------

To install the required packages, run the following command::

    pip install -r requirements.txt

This will install the packages listed under install_requires. For installing development, documentation, or Debian packaging dependencies, use the following format::

    pip install -r requirements.txt[dev]
    pip install -r requirements.txt[doc]
    pip install -r requirements.txt[deb]

Development
-----------

For development purposes, the boilerplate includes testing frameworks (nose2, unittest2) and a coverage tool (coverage). It also comes with virtualenv for creating isolated Python environments.

Documentation
-------------

To generate documentation, sphinx is listed as a dependency. It is a powerful documentation generator that converts reStructuredText files into HTML.

Debian Packaging
----------------

For Debian packaging, stdeb is included. This utility helps in creating Debian source packages from Python packages.

Contributing
------------

Contributions are welcome. Please read our contributing guidelines and submit pull requests to our repository.

Contact
-------

For any inquiries, please contact us at office@codev-it.at.

Donations
---------

Your support is appreciated! You can make a donation using the following methods:

Stripe
^^^^^^

For donations via credit or debit card, please visit our Stripe donation page:
`Donate with Stripe <https://donate.stripe.com/7sIaFa9DK18s8Ra8ww>`_

PayPal
^^^^^^

To donate using PayPal, please use the following link:
`Donate with PayPal <https://www.paypal.com/donate/?hosted_button_id=2GKPNJBQTAB3Y>`_

Cryptocurrency
^^^^^^

We also accept donations in cryptocurrency. Please send your contributions to our crypto account:
- **Bitcoin (BTC):** `bc1qadle9lzzv5plw98n0gndycx0dta2zyca9rmaxe`
- **Ethereum (ETH):** `0xf7a42B686437FA1b321708802296342D582576a0`

Contact
-------

For any inquiries, please contact us
at `office@codev-it.at <mailto:office@codev-it.at>`_.
