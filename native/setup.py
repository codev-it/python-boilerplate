# -*- coding: utf-8 -*-
"""
Codev-IT Python SetupTool
https://setuptools.readthedocs.io/en/latest/setuptools.html
"""
import os

# always prefer setuptools over distutils
from setuptools import setup, find_packages

try:
    from scripts.setup_command import BuildInfo
except ImportError:
    BuildInfo = None

with open(os.path.abspath('README.rst')) as f:
    readme = f.read()

with open(os.path.abspath('LICENSE.txt')) as f:
    licens = f.read()

setup(
    # Pull long description from the Readme file
    long_description=readme,

    # Choose your license
    license=licens,

    # You can just specify the packages manually here if your project is
    # simple. Or you can use find_packages().
    packages=find_packages('src'),
    package_dir={'': 'src'},
    include_package_data=True,

    # Include extension commands
    # https://dankeder.com/posts/adding-custom-commands-to-setup-py/
    cmdclass={'build_info': BuildInfo}
)
