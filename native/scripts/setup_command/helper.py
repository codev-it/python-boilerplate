# -*- coding: utf-8 -*-
from os import path


def write_requirement_file(setupcfg):
    """ create requirements info """

    requirement_file = path.abspath('requirements.txt')
    requirements = ['setup_requires', 'install_requires', 'extras_require']
    opt = setupcfg.get('options')
    with open(requirement_file, 'w') as f:
        f.write('# Automatically created. Please do not edit.\n')
        for requirement in requirements:
            items = opt.get(requirement)
            if type(items) == list:
                f.write('# {}\n'.format(requirement))
                for item in items:
                    f.write('%s\n' % item)
            if type(items) == dict:
                for key, item in items.items():
                    f.write('# {} {}\n'.format(requirement, key))
                    for value in item:
                        f.write('%s\n' % value)


def write_meta_file(setupcfg, status='alpha'):
    """ create meta info """

    meta_data = setupcfg.get('metadata')
    meta_file = path.join(path.abspath(''), 'src', meta_data.get('name'),
                          '__meta__.py')
    copyright = 'Copyright (c) {}'.format(meta_data.get('name'))
    with open('LICENSE.txt') as f:
        license = f.readline().replace('\n', '')
    meta_content = """# -*- coding: utf-8 -*-
# Automatically created. Please do not edit.
__namespace__ = '{namespace}'
__description__ = '{description}'
__version__ = '{version}'
__author__ = '{author}'
__copyright__ = '{copyright}'
__license__ = '{license}'
__maintainer__ = '{maintainer}'
__email__ = '{email}'
__status__ = '{status}'
""".format(namespace=meta_data.get('name'),
           description=meta_data.get('description'),
           version=meta_data.get('version'),
           author=meta_data.get('author'),
           copyright=copyright,
           license=license,
           maintainer=meta_data.get('maintainer'),
           email=meta_data.get('author_email'),
           status=status)

    with open(meta_file, 'w') as f:
        f.write(meta_content)
