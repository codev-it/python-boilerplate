# -*- coding: utf-8 -*-

import distutils.command.check
from setuptools.config import read_configuration

from scripts.setup_command import helper


class BuildInfo(distutils.command.check.check):
    """Specialized Python source metadata builder."""

    description = 'build the pkg metadata info'

    user_options = [
        ('status=', 's', 'build state default: planning')
    ]

    setupcfg = read_configuration('setup.cfg')

    def initialize_options(self):
        self.status = None

    def finalize_options(self):
        if self.status is None:
            self.status = 'planning'

    def run(self):
        helper.write_requirement_file(setupcfg=self.setupcfg)
        helper.write_meta_file(setupcfg=self.setupcfg, status=self.status)
