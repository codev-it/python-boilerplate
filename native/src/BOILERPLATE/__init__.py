# -*- coding: utf-8 -*-

from __future__ import absolute_import

import sys

from .__meta__ import __author__
from .__meta__ import __copyright__
from .__meta__ import __email__
from .__meta__ import __license__
from .__meta__ import __maintainer__
from .__meta__ import __status__
from .__meta__ import __version__

__all__ = ['BOILERPLATE']
__version__ = __version__
__author__ = __author__
__copyright__ = __copyright__
__license__ = __license__
__maintainer__ = __maintainer__
__email__ = __email__
__status__ = __status__


class BOILERPLATE:
    """"""

    def __init__(self, init=True):
        # attr
        self.init = init

    @staticmethod
    def run():
        """run()
        :return: sys.exit(0)
        :rtype: exit statement
        """
        print('Hallo Welt')
        return sys.exit(0)
