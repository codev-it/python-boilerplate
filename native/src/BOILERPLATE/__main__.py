# -*- coding: utf-8 -*-

from BOILERPLATE import BOILERPLATE


def main():
  """main()
  :return: BOILERPLATE.run()
  :rtype: exit statement
  """

  # init BOILERPLATE
  app = BOILERPLATE(init=True)
  return app.run()


if __name__ == "__main__":
  main()
