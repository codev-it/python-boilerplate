# -*- coding: utf-8 -*-
# Automatically created. Please do not edit.
__namespace__ = 'BOILERPLATE'
__description__ = 'Codev-IT Python BOILERPLATE'
__version__ = '0.0.1'
__author__ = 'Codev-IT'
__copyright__ = 'Copyright (c) BOILERPLATE'
__license__ = 'MIT License'
__maintainer__ = 'Codev-IT'
__email__ = 'office@codev-it.at'
__status__ = 'planning'
